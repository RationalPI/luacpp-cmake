#include <LuaCpp.hpp>
#include <iostream>

int main(int argc, char **argv) {
	struct LuaPredicatEvaluator{
		LuaCpp::LuaContext ctx;
		std::vector<std::string> errors;
		std::shared_ptr<LuaCpp::Engine::LuaTBoolean> returnState;
		bool compile(std::string luaExpression){
			returnState = std::make_shared<LuaCpp::Engine::LuaTBoolean>(false);
			ctx.AddGlobalVariable("ret", returnState);

			try {
				ctx.CompileString("test", "ret = ("+luaExpression+")");
			} catch (std::exception& e){
				errors.push_back(e.what());
				ctx=LuaCpp::LuaContext();
				return false;
			}
			return true;
		}
		bool value(){
			try {
				ctx.Run("test");
			}
			catch (std::exception& e){
				errors.push_back(e.what());
				ctx=LuaCpp::LuaContext();
				return false;
			}

			return returnState->getValue();
		}
	};

	LuaPredicatEvaluator lua;
	if(lua.compile("255==255")){
		std::cout << lua.value() << std::endl;
	}else {
		for (auto& err : lua.errors) {
			std::cout << err << std::endl;
		}
	}
}
